import { ApolloServer } from 'apollo-server';
import { connection } from './configs/connection.config';
import { dbInit } from './configs/init';
import { typeDefs } from './schemas/books-schema';
import { resolvers } from './schemas/books-schema/books.resolvers';

const server = new ApolloServer({
  typeDefs,
  resolvers
});


async function main() {
  await connection();
};

main().then(res => server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
}));