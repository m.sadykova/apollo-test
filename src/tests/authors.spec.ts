import { ApolloServer } from "apollo-server";
import { resolvers, typeDefs } from "../schemas/authors-schema";

describe("Query getAuthor", () => {

  it("should return authors", async () => {
    const server = new ApolloServer({
      typeDefs,
      resolvers,
    });
    const { data, errors } = await server.executeOperation({
      query: `query ExampleQuery {
        getAuthors {
            id
            name
        }
      }`
    });

    expect(errors).toBeUndefined();

    expect(data).toBeTruthy();

    expect(data!.getAuthors.length).toBeTruthy();

    expect(data!.getAuthors[0]).toHaveProperty('name');
    expect(data!.getAuthors[0]).toHaveProperty('id');

    expect(data!.getAuthors[0].id).toBe(null); // разобраться с точки зрения ДБ

    expect(data!.getAuthors[0].name).toEqual(expect.any(String));
  });
});