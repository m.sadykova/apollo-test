import { ApolloServer } from "apollo-server";
import { resolvers, typeDefs } from "../schemas/books-schema";

describe("Query getBooks", () => {

  it("should return books", async () => {
    const server = new ApolloServer({
      typeDefs,
      resolvers,
    });
    const { data, errors } = await server.executeOperation({
      query: `query ExampleQuery {
        getBooks {
          id
          title
        }
      }`
    });

    expect(errors).toBeUndefined();

    expect(data).toBeTruthy();

    expect(data!.getBooks.length).toBeTruthy();

    expect(data!.getBooks[0]).toHaveProperty('id');
    expect(data!.getBooks[0]).toHaveProperty('title');

    expect(data!.getBooks[0].id).toEqual(expect.any(Number));
    expect(data!.getBooks[0].title).toEqual(expect.any(String));
  });
});
