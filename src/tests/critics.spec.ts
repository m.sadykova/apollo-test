import { ApolloServer } from "apollo-server";
import { resolvers, typeDefs } from "../schemas/critics-schema";

describe("Query getCritics", () => {

    it("should return critics", async () => {
        const server = new ApolloServer({
            typeDefs,
            resolvers,
        });
        const { data, errors } = await server.executeOperation({
        query: `query ExampleQuery {
              getCritics {
                  id
                  title
              }
            }`
        });

        expect(errors).toBeUndefined();

        expect(data).toBeTruthy();

        expect(data!.getCritics.length).toBeTruthy();

        expect(data!.getCritics[0]).toHaveProperty('id');
        expect(data!.getCritics[0]).toHaveProperty('title');

        expect(data!.getCritics[0].id).toEqual(expect.any(Number));
        
        expect(data!.getCritics[0].title).toBe(null); // разобраться с точки зрения ДБ
    });
});