import { ApolloServer } from "apollo-server-express";
import { envConfig } from "../../configs/envConfig";
import { BookI } from "entities/books";
import { classToInvokable } from "sequelize/types/lib/utils";
import { resolvers, typeDefs } from "../../schemas/books-schema";

declare global {
    namespace jest {
        interface Matchers<R> {
            toBeWithinRange(a: number, b: number): R;
        }
    }
};

describe('Simple expects testing', () => {
    const server = new ApolloServer({
        typeDefs,
        resolvers,
    });

    it('1, expect.extend(matcher)', async () => {
        const { data, errors } = await server.executeOperation({
            query: `query ExampleQuery {
                      getBooks {
                        id
                        title
                      }
                    }`,
            variables: { id: 1 },
        });
        //expect.extend(matcher) - describe matcher
        expect.extend({
            toBeWithinRange(received, min, max) {
                const pass = received >= min && received <= max;
                if (pass) {
                    return {
                        message: () =>
                            `expected ${received} not to be within range ${min} - ${max}`,
                        pass: true,
                    };
                } else {
                    return {
                        message: () =>
                            `expected ${received} to be within range ${min} - ${max}`,
                        pass: false,
                    };
                }
            },
        });
        // expect.extend(matcher) allow add custom matchers to Jest
        expect(data!.getBooks.length).toBeWithinRange(1, envConfig.max_books_amount + 1);

    });

    it('2, .toEqual() and expect.anything()', async () => {
        const { data, errors } = await server.executeOperation({
            query: `query ExampleQuery {
                          getBooks {
                            id
                            title
                          }
                        }`,
            variables: { id: 1 },
        });
        // expect.anything() matches anything but null or undefined
        expect(data!.getBooks).toEqual(expect.anything());

    });

    it('3, 4, 5 expect.arrayContaining(array), expect.not.arrayContaining(array), .toEqual(value)', () => {
        // expect.arrayContaining(expected) matches a received array, 
        // which contains all of the elements in the expected array;
        // .toEqual() check "deep" equality
        const fullNumbers: number[] = [1, 2, 3, 4, 5];
        let partNumbers: number[] = fullNumbers.slice(1, 3);
        expect(fullNumbers).toEqual(expect.arrayContaining(partNumbers));
        expect(partNumbers).toEqual(expect.not.arrayContaining(fullNumbers));
    });

    it('6, expect.any(constructor)', async () => {
        const { data, errors } = await server.executeOperation({
            query: `query ExampleQuery {
                          getBooks {
                            id
                            title
                          }
                        }`,
            variables: { id: 1 },
        });
        // expect.any(constructor) matches anything that was created with the given constructor
        expect(data!.getBooks.pop()).toEqual(expect.any(Object))
    });

    it('7, 8, .toBe(value), .not', async () => {
        const sum = 5;
        // .toBe(value) - compare primitive values or to check referential identity of object instances
        expect(2 + 3).toBe(sum);
        // .not - test opposite something.
        expect(3 + 3).not.toBe(sum);
    });

    it('9, 10, .expect.not.objectContaining(object), expect.objectContaining(object)', () => {
        const man = {
            name: 'John',
            age: 40
        };
        // expect.not.objectContaining(object) - matches any received object
        // that does not recursively match the expected properties
        expect({ age: 40 }).toEqual(expect.not.objectContaining(man));
        //expect.objectContaining(object) - matches any received object 
        //that recursively matches the expected properties
        expect(man).toEqual(expect.objectContaining({ age: 40 }));
    });

    it('11, 12, expect.not.stringContaining(string), expect.stringContaining(string)', () => {
        const expected = 'hello';
        // expect.not.stringContaining(string) - matches the received value if it is not a string 
        // or if it is a string that does not contain the exact expected string.
        expect('hi world').toEqual(expect.not.stringContaining(expected));
        // expect.stringContaining(string)-  matches the received value if it is a string 
        // that contains the exact expected string.
        expect('hello world').toEqual(expect.stringContaining(expected));
    });

    it('13, 14, expect.not.stringMatching(string | regexp), expect.stringMatching(string | regexp)', () => {
        const expectedRegexp = /hello world/;
        const expectedString = 'hello world';
        // expect.not.stringMatching(string | regexp) matches the received value if it is not a string 
        // or if it is a string that does not match the expected string or regular expression.
        expect('123').toEqual(expect.not.stringMatching(expectedRegexp));
        expect('123').toEqual(expect.not.stringMatching(expectedString));
        // expect.stringContaining(string) matches the received value if it is a string 
        // that contains the exact expected string.
        expect('hello world 123').toEqual(expect.stringMatching(expectedRegexp));
        expect('hello world 123').toEqual(expect.stringMatching(expectedString));
    });

    it('15, .toHaveLength(number)', () => {
        const numericArray = [0, 1, 2, 4, 5];
        // .toHaveLength(number) - check that an object 
        // has a .length property and it is set to a certain numeric value.
        expect(numericArray).toHaveLength(5);
    });

    it('16, .toHaveProperty(keyPath, value?)', () => {
        const rabbit = {
            alive: true,
            color: 'white',
            organs: {
                ear: 2,
                heart: 1
            },
        }
        // .toHaveProperty(keyPath, value?) - check if property at provided reference keyPath exists for an object
        expect(rabbit).toHaveProperty('alive');
        expect(rabbit).toHaveProperty('color', 'white');
        expect(rabbit).toHaveProperty('organs.ear', 2);
    });

    it('17, .toBeCloseTo(number, numDigits?)', () => {
        // .toBeCloseTo(number, numDigits?) - compare floating point numbers for approximate equality.
        expect(0.034 + 0.89).toBeCloseTo(0.923); // actually 0.924
    });

    it('18, 19, .toBeDefined(), .toBeUndefined()', () => {
        let definedVariable = 1;
        let undefinedVariable;
        const returnsNoValue = () => {
            1 + 2;
        }
        // .toBeDefined - check that a variable is not undefined
        expect(definedVariable).toBeDefined();
        expect(returnsNoValue()).not.toBeDefined();
        expect(undefinedVariable).not.toBeDefined();
        // .toBeUndefined() - check that a variable is undefined
        expect(returnsNoValue).toBeDefined();

    });

    it('20, 21, .toBeFalsy(), .toBeTruthy()', () => {
        const returnFalse = (a: number, b: number): boolean => {
            if (typeof (a + b) === 'string') {
                return true
            } else {
                return false
            }
        };
        // .toBeFalsy() - ensure a value is false in a boolean context
        expect(returnFalse(1, 2)).toBeFalsy();
        // .toBeTruthy() - ensure a value is true in a boolean context
        expect(returnFalse(3, 4)).not.toBeTruthy();
    });

    it('22, 23, 24, 25, .toBeGreaterThan(number | bigint), .toBeGreaterThanOrEqual(number | bigint), .toBeLessThan(number | bigint), .toBeLessThanOrEqual(number | bigint)', () => {
        const sum = (a: number, b: number) => {
            return a + b;
        };
        const target = 10;
        // .toBeGreaterThan - compare received > expected for number or big integer values
        expect(sum(12, 3)).toBeGreaterThan(target);
        // .toBeGreaterThanOrEqual - compare received >= expected for number or big integer values
        expect(sum(10, 0)).toBeGreaterThanOrEqual(target);
        // .toBeLessThan(number | bigint) - compare received < expected for number or big integer values
        expect(sum(1, 3)).toBeLessThan(target);
        // .toBeLessThanOrEqual(number | bigint) -  compare received <= expected for number or big integer values
        expect(sum(7, 3)).toBeLessThanOrEqual(target);
    });

    it('26, .toBeInstanceOf(Class)', () => {
        // .toBeInstanceOf(Class) - check that an object is an instance of a class
        expect([]).toBeInstanceOf(Array)
    });

    it('27, .toBeNull', () => {
        const returnsNull = () => null;
        //.toBeNull() is the same as .toBe(null) but the error messages are a bit nicer. 
        expect(returnsNull()).toBeNull();
    });

    it('28, .toBeNaN', () => {
        const stringOperand = 'a';
        //@ts-ignore
        const nanValue = 1 * 'stringOperand';
        //.toBeNaN - check a value is NaN.
        expect(nanValue).toBeNaN();
    });

    it('29, .toContain(item)', () => {
        const numbers = [1, 2, 3, 4];                              // работает со структурой "массив без вложенных объектов" 
        // .toContain - check that an item is in an array.         // и проверяет наличие конкретного элемента
        expect(numbers).toContain(2);
    });

    it('30, .toContainEqual(item)', () => {
        const apples = { red: 1, yellow: 2, h: 'green' };          // работает со структурой "массив с вложенными объектами" 
        const cars = [{ red: 1, yellow: 2, h: 'green' }, { a: "b" }];  // и проверяет наличие вложенного объекта в другом массиве 
        //.toContainEqual when you want to check that an item      // с такой же структурой либо совпадение вложенного с указанным в expect
        //with a specific structure and values is contained in an array.
        expect(cars).toContainEqual(apples);
        expect(cars).toContainEqual({ red: 1, yellow: 2, h: 'green' });
    });

    it('31, .toEqual(value)', () => {
        const apples = ['red', 'yellow', 'green'];
        const cars = ['red', 'yellow', 'green'];
        // .toEqual to compare recursively all 
        // properties of object instances (also known as "deep" equality)
        expect(cars).toEqual(apples);
    });

    it('32, .toMatch(regexp | string)', () => {
        const regexp = /hello/;
        const testingString = 'hello world';
        //.toMatch to check that a string matches a regular expression.
        expect(testingString).toMatch(regexp);
    });

    it('33, .toMatchObject(object)', () => {
        const rabbit = {
            alive: true,
            color: 'black',
            organs: {
                ear: 2,
                heart: 1
            },
        };
        const mammoth = {
            color: 'black',
            organs: {
                ear: 2,
                heart: 1
            },
        };
        // .toMatchObject - check that a JavaScript object 
        // matches a subset of the properties of an object.
        expect(rabbit).toMatchObject(mammoth);
    });

    it('34, .toStrictEqual(value)', () => {
        //.toStrictEqual - test that objects have the same types as well as structure.
        expect({ a: undefined, b: 2 }).not.toStrictEqual({ b: 2 });
        expect([undefined, 1]).not.toStrictEqual([, 1]);
    });

    it('35, .toThrow(error?)', () => {
        const throwsError = () => {
            if (true) {
                throw 'arguments is undefined'
            }
        };
        // .toThrow - test that a function throws when it is called
        expect(throwsError).toThrow('arguments is undefined');
    });
});





