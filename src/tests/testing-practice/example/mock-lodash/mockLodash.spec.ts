import lodash from 'lodash'
import axios from 'apollo-server'




import { getUsers, getUserData } from './users';

jest.mock('lodash');

const avatarGetter = lodash.toNumber as jest.Mock;

beforeEach(() => {
    avatarGetter.mockReset();
  });
  
  describe('getUsers', () => {
    it('should call the API and return the data', async () => {
      const usersData = [
        { id: 51, name: 'Allan' },
        { id: 120, name: 'George' },
      ];
      console.log(avatarGetter)
      avatarGetter.mockResolvedValue({ data: usersData });
      console.log(avatarGetter)
      // also could be mockImplementation 
      // or anything that mock functions can do
  
      const returnedUsersData = await getUsers();
      console.log('----',returnedUsersData)
  
      expect(returnedUsersData).toEqual(usersData);
      // and because axios.get is a mock function, we can assert it too
      expect(avatarGetter).toHaveBeenCalledTimes(1);
      expect(avatarGetter).toHaveBeenCalledWith('/api/users');
    });
  });