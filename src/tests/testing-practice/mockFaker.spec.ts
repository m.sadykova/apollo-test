import Faker from 'faker';

jest.mock('faker', () => ({
    datatype: {
        number: jest.fn().mockImplementation(() => 10),
    },
}));

let FakerMock: any;

describe('Faker', () => {
    beforeEach(() => {
        FakerMock = Faker;
    });
    it('should have been called', () => {
        FakerMock.datatype.number(3);
        expect(FakerMock.datatype.number).toHaveBeenCalledTimes(1);
        expect(FakerMock.datatype.number).toHaveBeenCalledWith(3);
        expect(FakerMock.datatype.number(2)).toBe(10)
    });
});