describe('Asynchronous expects testing', () => {

    it('1, expect.assertions(number)', (done) => {
        // expect.assertions(number) verifies that a certain number of assertions 
        //are called during a test. 
        expect.assertions(2);
        const callbackSum = () => {
            expect(true).toBe(true);
        };
        const callbackDifference = () => {
            expect(true).toBe(true);
        };

        const returnCallbacksMultiply = (callbackSum: () => void, callbackDifference: () => void) => {

            return new Promise((resolve, reject) => {
                setTimeout(() => { callbackSum(); callbackDifference(); resolve(true) }, 500);
            });
        };
        returnCallbacksMultiply(callbackSum, callbackDifference).then(() => done());
    });

    it('2, expect.hasAssertions()', () => {
        // expect.hasAssertions() verifies that at least one assertion is called during a test.
        expect.hasAssertions();
        const returnSum = async () => {
            return expect(3 + 3).toBe(6);
        };
        returnSum();
    });

    it('3, .resolves', () => {
        const promiseSum = (a: number, b: number) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => resolve(a + b), 1000);
            });
        };
        // .resolves - unwrap the value of a fulfilled promise so any other matcher can be chained
        return expect(promiseSum(3, 3)).resolves.toBe(6);
    });

    it('4, .rejects', () => {
        const promiseSum = (a: number, b: number) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => reject('Слишком долгое ожидание'), 1000);
            });
        };
        // .rejects - unwrap the reason of a rejected promise so any other matcher can be chained.
        return expect(promiseSum(5, 2)).rejects.toEqual('Слишком долгое ожидание');
    });
});
