describe('Mocks testing', () => {
    const mockIncrement = jest.fn((elem) => elem++);
    const numbers = [1, 2, 3, 4];
    for (let i = 0; i < numbers.length; i++) {
        mockIncrement(numbers[i])
    };
    it('1, .toHaveBeenCalled()', () => {
        //.toHaveBeenCalled - ensure that a mock function got called.
        expect(mockIncrement).toHaveBeenCalled();
    });

    it('2, .toHaveBeenCalledTimes(number)', () => {
        //.toHaveBeenCalledTimes to ensure that a mock function got called exact number of times
        expect(mockIncrement).toHaveBeenCalledTimes(numbers.length);
    });

    it('3,toHaveBeenCalledWith', () => {
        //.toHaveBeenLastCalledWith - ensure that a mock function is called with specific arguments.
        expect(mockIncrement).toHaveBeenCalledWith(3);        //за раз проверяет только один аргумент
    });

    it('4, toHaveReturned', () => {
        //.toHaveReturned - test that the mock function successfully returned (i.e., did not throw an error) at least one time
        expect(mockIncrement).toHaveReturned();
    });

    it('5, toHaveBeenLastCalledWith', () => {
        //.toHaveBeenLastCalledWith - test what arguments it was last called with
        expect(mockIncrement).toHaveBeenLastCalledWith(numbers[3]);
    });

    it('6, toHaveBeenNthCalledWith', () => {
        //.toHaveBeenNthCalledWith - test what arguments it was nth called with
        expect(mockIncrement).toHaveBeenNthCalledWith(1, 1);
    });

    it('7, .toHaveReturnedTimes(number)', () => {
        //.toHaveReturnedTimes to ensure that a mock function returned successfully 
        //(i.e., did not throw an error) an exact number of times
        expect(mockIncrement).toHaveReturnedTimes(numbers.length);
    });

    it('8, .toHaveReturnedWith(value)', () => {
        //.toHaveReturnedWith to ensure that a mock function returned a specific value.
        expect(mockIncrement).toHaveReturnedWith(2);
    });

    it('9, .toHaveNthReturnedWith(nthCall, value)', () => {
        //toHaveNthReturnedWith - test the specific value that a mock function returned for the nth call
        expect(mockIncrement).toHaveNthReturnedWith(1, 1);
    });

});

describe('Other simple mock functions testing', () => {
    const newMock = jest
        .fn()
        .mockImplementation((elem) => elem--)
        .mockName('mockDecrement');
    const numbers = [1, 2, 3, 4];
    for (let i = 0; i < numbers.length; i++) {
        newMock(numbers[i])
    };
    it('1', () => {
        expect(newMock.getMockName()).toBe('mockDecrement');
    });

    it('2', () => {
        expect(newMock.mock.calls).toContainEqual([1]);
    });

    it('3', () => {
        expect(newMock.mock.calls).toEqual([[1], [2], [3], [4]]);
    });

    describe('Mock methods', () => {
        const newMock = jest
            .fn()
            .mockImplementation((elem) => elem--)
            .mockName('mockDecrement');
        const numbers = [1, 2];
        for (let i = 0; i < numbers.length; i++) {
            newMock(numbers[i])
        };

        it('1, .getMockName()', () => {
            expect(newMock.getMockName()).toBe('mockDecrement');
        });

        it('2, .mock.calls', () => {
            expect(newMock.mock.calls[0][0]).toBe(1)
        });

        it('3, .mock.results', () => {
            expect(newMock.mock.results).toEqual([{ "type": "return", "value": 1 }, { "type": "return", "value": 2 }]);
            expect(newMock.mock.results).not.toEqual([{ "type": "throw", "value": 1 }, { "type": "incomplete", "value": 2 }]);
        });

        it('4, .mock.instances', () => {
            expect(newMock.mock.instances[0]).toBeUndefined();
        });

        //.mockClear()
        //.mockReset()
        //.mockRestore()
        //.mockImplementation(fn)
        //.mockImplementationOnce(fn)
        //.mockName(value)
        //.mockReturnThis()
        //.mockReturnValue(value)
        //.mockReturnValueOnce(value)
        //.mockResolvedValue(value)
        //.mockResolvedValueOnce(value)
        //.mockRejectedValue(value)
        //.mockRejectedValueOnce(value)
        //.mockReturnThis()
        //.mockReturnThis()

    });
});