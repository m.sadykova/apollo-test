import { ApolloServer } from 'apollo-server';
jest.mock('apollo-server', () => {
    return {
        ApolloServer: {
            name: '123'
        }
    }
});

test('', () => {
    expect(ApolloServer.name).toEqual('123');
});