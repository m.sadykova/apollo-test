import { envConfig } from '../configs/envConfig';
import Faker from 'faker';

export interface Critic {
    id: number,
    name: string
};

export const critics: Critic[] = [];
const randomCriticsAmount = Faker.datatype.number(
    { min: 1, max: envConfig.max_critics_amount }
);
for (let i = 0; i <= randomCriticsAmount; i++) {
    const randomCriticId = Faker.datatype.number(
        50
    );
    const randomName = Faker.random.words(
        2
    );
    const critic: Critic = {
        id: randomCriticId,
        name: randomName,
    };
    critics.push(critic)
};