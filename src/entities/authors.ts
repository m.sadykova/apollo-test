import { envConfig } from '../configs/envConfig';
import Faker from 'faker';

export interface Author {
    name: string
};

export const authors: Author[] = [];
const randomAuthorsAmount = Faker.datatype.number(
    { min: 1, max: envConfig.max_authors_amount }
);
for (let i = 0; i <= randomAuthorsAmount; i++) {
    const randomName = Faker.random.words(
        2
    );
    const author: Author = {
        name: randomName,
    };
    authors.push(author)
};