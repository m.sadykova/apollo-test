import { envConfig } from '../configs/envConfig';
import Faker from 'faker';

const randomBooksAmount = Faker.datatype.number(
  { min: 1, max: envConfig.max_books_amount }
);
export interface BookI {
  id: number,
  title: string
}

export const books: BookI[] = [];

for (let i = 0; i <= randomBooksAmount; i++) {
  const randomBookId = Faker.datatype.number(
    50
  );
  const randomTitle = Faker.random.words(
    2
  );
  const book: BookI = {
    id: randomBookId,
    title: randomTitle,
  };
  books.push(book)
};

