import { sequelizeConnection } from "../../configs/connection.config";
import { DataTypes, Model } from "sequelize";
import { AuthorAttributesI } from "./author.model";

export interface BookAttributesI {
  id: number;
  title: string;
}

export type BookInputT = BookAttributesI;
export type BookOuputT = BookAttributesI;

export class Book
  extends Model<BookAttributesI, BookInputT> implements BookAttributesI
{
  public id: number;
  public title: string;
  public author: string;
}

Book.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize: sequelizeConnection,
    modelName: "Book",
  }
);
