import { sequelizeConnection } from "@connection/connection.config";
import { DataTypes, Model } from "sequelize";

export interface AuthorAttributesI {
    id: number;
    name: string
};

export type AuthorInputT = AuthorAttributesI;
export type AuthorOuputT = AuthorAttributesI;

export class Author extends Model<AuthorAttributesI, AuthorInputT> implements AuthorAttributesI {
    public id: number;
    public name: string
};

Author.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING
    }
}, {
    sequelize: sequelizeConnection,
    modelName: 'Author'
});