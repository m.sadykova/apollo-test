import { envConfig } from "../../configs/envConfig";

const local = {
  database: `${envConfig.pg_db_name}`,
  host: "127.0.0.1",
  dialect: "postgres",
  port: `${envConfig.pg_port}`,
  password: `${envConfig.pg_password}`,
  username: `${envConfig.pg_username}`,
};

const database = Object.assign({}, local, {
  dialect: "postgres",
  logging: false,
  pool: {
    max: 15,
    min: 0,
    acquire: 60000,
    idle: 10000,
  },
});

/* Sequelize uses CommonJS modules */
module.exports = {
  test: database,
  testing: database,
  local: database,
};
