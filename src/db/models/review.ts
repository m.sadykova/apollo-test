'use strict';
import { Model, Sequelize, DataTypes } from "sequelize";

const TARGET_MODEL_CRITIC = "critic";
const TARGET_MODEL_BOOK = "book";

export default (sequelize: Sequelize) => {
  class Review extends Model {
    static associate(models: any) {
      models.JunctionTable.belongsTo(models.Critic, {
        foreignKey: 'criticId',
        targetKey: 'criticId',
        as: TARGET_MODEL_CRITIC
      });
      models.Critic.belongsTo(models.Book, {
        foreignKey: 'bookId',
        targetKey: 'bookId',
        as: TARGET_MODEL_BOOK
      });
    }
  };
  Review.init({
    id: DataTypes.NUMBER
  }, {
    sequelize,
    modelName: 'Review',
  });
  return Review;
};