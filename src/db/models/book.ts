"use strict";
import { Model, Sequelize, DataTypes } from "sequelize";

export default (sequelize: Sequelize) => {
  const TARGET_MODEL = "author";
  class Book extends Model {
    static associate(models: any) {
      models.Book.belongsTo(models.Author, {
        foreignKey: "authorId",
        targetKey: "id",
        as: TARGET_MODEL,
      });
      models.Book.belongsToMany(models.Critic, {
        through: models.Review,
        foreignKey: 'bookId',
        as: 'CriticsForBook',
      });
    }
  }
  Book.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER,
        defaultValue: DataTypes.UUID
      },
      title: DataTypes.STRING,
      authorId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Book",
    }
  );
  return Book;
};
