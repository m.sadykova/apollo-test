'use strict';
import { Model, Sequelize, DataTypes } from "sequelize";

const TARGET_MODEL = "book";

export default (sequelize: Sequelize) => {
  class Critic extends Model {
    static associate(models: any) {
      models.Critic.belongsToMany(models.Book, {
        through: models.Review,
        foreignKey: 'critic_id',
        as: 'BooksForCritic',
      });
    }
  };
  Critic.init({
    id: DataTypes.NUMBER,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Critic',
  });
  return Critic;
};