"use strict";
import { Model, Sequelize, DataTypes } from "sequelize";

export default (sequelize: Sequelize) => {
  const TARGET_MODEL = "books";

  class Author extends Model {
    static associate(models: any) {
      models.Author.hasMany(models.Book, {
        foreignKey: "authorId",
        as: TARGET_MODEL,
      });
    }
  }
  Author.init(
    {
      id: {
        allowNull: false,
        // autoIncrement: true,
        primaryKey: true,
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUID
      },
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Authors",
    }
  );
  return Author;
};
