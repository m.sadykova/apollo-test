"use strict";

import fs from "fs";
import path from "path";
import { Sequelize, DataTypes } from "sequelize";

import createAuthor from "./models/author";
import createBook from "./models/book";

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || "local";
const config = require(__dirname + "/config")[env];

let sequelize: Sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

const db = {
  Author: createAuthor(sequelize),
  Book: createBook(sequelize),
};

export type DbType = typeof db & {
  sequelize: Sequelize;
};

export type DbModels = typeof db;

Object.keys(db).forEach((modelName) => {
  const internalDb = db as DbType & { [key: string]: any };
  if (internalDb[modelName].associate) {
    internalDb[modelName].associate(db);
  }
});

const exportDb: DbType = {
  ...db,
  sequelize,
};

export { exportDb as db };
