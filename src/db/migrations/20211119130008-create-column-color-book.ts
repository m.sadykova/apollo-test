'use strict';

import { DataTypes, QueryInterface, Sequelize } from "sequelize";

export default {
  up: async (queryInterface: QueryInterface, Sequelize: Sequelize) => {
    await queryInterface.addColumn("Books", "color", {
      type: DataTypes.STRING
    })
  },

  down: async (queryInterface: QueryInterface, Sequelize: Sequelize) => {
    await queryInterface.removeColumn("Books", "color")
  }
};
