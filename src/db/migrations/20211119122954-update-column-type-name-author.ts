'use strict';

import { DataTypes, QueryInterface, Sequelize } from "sequelize";

export default {
  up: async (queryInterface: QueryInterface, Sequelize: Sequelize) => {
    await queryInterface.changeColumn("Authors", "name", {
      type: DataTypes.TEXT
    })
  },

  down: async (queryInterface: QueryInterface, Sequelize: Sequelize) => {
    await queryInterface.changeColumn("Authors", "name", {
      type: DataTypes.STRING
    })
  }
};
