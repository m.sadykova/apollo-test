'use strict';
import { Sequelize, QueryInterface, DataTypes } from "sequelize";

export default {
  up: async (queryInterface: QueryInterface, Sequelize: Sequelize) => {
    queryInterface.removeColumn("Books", "color")
  },

  down: async (queryInterface: QueryInterface, Sequelize: Sequelize) => {
    queryInterface.addColumn("Books", "color", {
      type: DataTypes.STRING
    })
  }
};
