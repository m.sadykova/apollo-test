import { envConfig } from './envConfig';
import { Sequelize } from 'sequelize';

export const sequelizeConnection = new Sequelize(`${envConfig.pg_db_name}`, `${envConfig.pg_username}`, `${envConfig.pg_password}`, {
    dialect: "postgres",
    host: "localhost",
    port: envConfig.pg_port
});

export const connection = async () => {
    try {
        await sequelizeConnection.authenticate();
        console.log('Соединение с БД успешно установлено')
    } catch (e) {
        console.log('Невозможно выполнить подключение к БД: ', e)
    };
};
