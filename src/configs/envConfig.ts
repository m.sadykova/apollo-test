const { parsed } = require("dotenv").config();

const envConfig = {
    env: String(parsed.NODE_ENV),
    port: parseInt(parsed.PORT),
    pg_db_name: String(parsed.PG_DB_NAME),
    pg_username: String(parsed.PG_USERNAME),
    pg_password: Number(parsed.PG_PASSWORD),
    pg_port: Number(parsed.PG_PORT),

    max_books_amount: Number(parsed.MAX_BOOKS_AMOUNT),
    max_authors_amount: Number(parsed.MAX_AUTHORS_AMOUNT),
    max_critics_amount: Number(parsed.MAX_CRITICS_AMOUNT)
}

export { envConfig };

