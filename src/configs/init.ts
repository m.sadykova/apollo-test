import { Author } from '../entities/models/author.model';
import { Book } from '../entities/models/book.model'

export const dbInit = async () => {
    try {
        await Book.sync();
        await Author.sync();
        console.log('Таблицы созданы')
    } catch (e) {
        console.log('Ошибка создания таблиц')
    }
}