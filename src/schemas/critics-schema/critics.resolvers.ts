import { critics } from "../../entities/critics";

export const resolvers = {
    Query: {
        getCritics: () => critics,
    },
}