import { gql } from "apollo-server";

export const typeDefs = gql`
  type Critic {
    id: Int
    title: String
  }

  type Query {
    getCritics: [Critic]
  }
`