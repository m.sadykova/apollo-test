import { gql } from "apollo-server";

export const typeDefs = gql`
  type Author {
    id: Int
    name: String
  }

  type Book {
    id: Int
    title: String
    author: Author
  }

  type Query {
    getBooks: [Book]
  }

  type Mutation {
    updateBooks(ids: [Int]): [Book]
    updateAuthors: String
  }
`;
