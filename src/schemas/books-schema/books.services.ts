import { books } from '../../entities/books';
import Faker from 'faker';
import { BookI } from '../../entities/books';

export interface UpdateBooksArgsI {
    ids: number[]
}

export const updateBook = (books: BookI[], { ids }: UpdateBooksArgsI) => {
    console.log('----', books)
    if (books !== null && books !== undefined) {
        console.log('----return args', ids)
        if (!ids.length) {
            return books
        }
        for (let i = 0; i < books.length; i++) {
            for (const value of ids) {
                books.reduce((prev, curr) => {
                    if (curr.id === value) {
                        curr.title = Faker.random.words(2)
                    }
                    return prev
                }, books);
            }
        }
        console.log('-----return books', books)
        return books
    } else {
        return null
    }
}