import { BookI } from "../../entities/books";
import { books } from "../../entities/books";
import { updateBook } from "./books.services";
import { UpdateBooksArgsI } from "./books.services";

export const resolvers = {
  Query: {
    getBooks: () => books,
  },
  Mutation: {
    updateBooks: (root: any, { ids }: UpdateBooksArgsI) =>
      updateBook(books, { ids }),
  },
  Book: {
    author: (root: BookI, args: { who: string }) => {
      return Object.assign({}, { name: args.who.toUpperCase() });
    },
  },
};
