import { authors } from "../../entities/authors";

export const resolvers = {
  Query: {
    getAuthors: () => authors,
  },
};
