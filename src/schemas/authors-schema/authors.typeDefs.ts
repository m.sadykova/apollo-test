import { gql } from "apollo-server";

export const AuthorT = `
type Author {
  id: Int
  name: String
}
`;

export const typeDefs = gql`
  ${AuthorT}

  type Query {
    getAuthors: [Author]
  }
`;
