import type { Config } from "@jest/types";
const config: Config.InitialOptions = {
  verbose: true,
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  transformIgnorePatterns: ["/node_modules/"],
  testRegex: "(/tests/.*|(\\.|/)(test|spec))\\.(ts)$",
  moduleFileExtensions: ["ts", "js", "json", "node"],
};
export default config;
