const path = require("path");
const { db } = require(path.resolve(process.cwd(), "dist/src/db-v2/index.js"));

async function clearDB(sequelize, environment) {
  try {
    console.log(sequelize);
    await sequelize.query("DROP SCHEMA public CASCADE");
    console.log("schema dropped");
    await sequelize.query("CREATE SCHEMA public");
    console.log("schema created");
    return await sequelize.close();
  } catch (e) {
    console.log(e.message);
    await sequelize.close();
    process.exit(1);
  }
}

clearDB(db.sequelize, "local").then(() => console.log("db cleared"));
